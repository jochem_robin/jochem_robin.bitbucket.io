window.addEventListener('load', init);

var newsTbody;
var search;
var input;

 function init() {
    newsTbody = document.getElementById('news');
    getArticle();

}


function getArticle() {
  var input = document.getElementById('userInput').value;
  var url = "https://api.nytimes.com/svc/search/v2/articlesearch.json";
  url += '?' + $.param({
    'api-key': "4eb951e64d5640d591090474242d6aa9",
    'q': input,
    'sort': "newest",
    'page': 0,

  });

    $.getJSON(url) 
    .done(successHandler)
    .fail(failHandler) 
}





  // .done

   function successHandler(data) {
    console.log('Successvol, hier de resultaten: ', data);

    var articles = data.response.docs;

    var ph = document.getElementById('news');
    for (var i = 0; i < articles.length; i++) {
        var element = articles[i];

        //TR
        var tr = document.createElement('tr');

        //TD Article Name
        var td_name = document.createElement('td');
        td_name.innerHTML = articles[i].headline.main;
        tr.appendChild(td_name);

        //TD Description
        var td_snippet = document.createElement('td');
        td_snippet .innerHTML = articles[i].snippet;
        tr.appendChild(td_snippet);

        //TD Words
        var td_words = document.createElement('td');
        td_words.innerHTML = articles[i].word_count;
        tr.appendChild(td_words);

        //TD URL
        var a = document.createElement('a');
        var linkText = document.createTextNode("Read more");
        a.appendChild(linkText);
        a.title = "nyTimes_url";
        a.href = articles[i].web_url;
        a.target = "_blank;"
        tr.appendChild(a);
        ph.appendChild(tr);

        // //TD Gepubliceerd
        
        // // Tijd moet nog gewijzigd worden van EDT naar CEST
        // // CEST is 6 uur later
        // // Verander tijd naar iets overzichtelijks

        // // Iets met:
        // // var timeEDT = pub_date.T (?)
        // // var timeCEST = timeEDT + 6 (?)
        
        // /* 
        // TEST BEREKENING EDT NAAR CEST 


        // */
        // var td_pub = document.createElement('td');
        // td_pub.innerHTML = articles[i].pub_date;
        // tr.appendChild(td_pub);

        ph.appendChild(tr);
    }
  }

  // .fail

  function failHandler(data) {
    console.log('Er is een fout opgetreden');
}





